<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Operations;

class OperationsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 10; $i++) { 
         $operation = new Operations;
         $operation->setCredit(mt_rand(10, 100));
         $operation->setDebit(mt_rand(300, 800));
         $operation->setDescription("description".$i);
         $operation->setDate(new \DateTime("07/09/2000"));

         $manager->persist($operation);
        }

        $manager->flush();
    }
}
