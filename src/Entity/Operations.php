<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OperationsRepository")
 */
class Operations
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $credit;

    /**
     * @ORM\Column(type="integer")
     */
    private $debit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $total;

    public function getId()
    {
        return $this->id;
    }

    public function getCredit(): ?int
    {
        return $this->credit;
    }

    public function setCredit(int $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    public function getDebit(): ?int
    {
        return $this->debit;
    }

    public function setDebit(int $debit): self
    {
        $this->debit = $debit;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate($date): self
    {
        if ($date instanceof \DateTime) {
            $this->date = $date;
        }
        elseif (is_string($date)) {
            $this->$date = \DateTime::fromFormat("d/m/Y", $date);
        }
        elseif (is_int($date)) {
            $this->date = new \DateTime();
            $this->date->setTimeStamp($date / 1000);
        }
        if(!$this->date instanceof \Datetime){
            throw new \InvalidArgumentException("Date must be d/m/y or time stamp or Datetime");
        }
        return $this;
    }

}
