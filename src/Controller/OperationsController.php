<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Operations;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class OperationsController extends Controller
{
  private $serializer;
  public function __construct(SerializerInterface $serializer)
  {
    $this->serializer = $serializer;
  }

  /**
   * @Route("/", methods="GET")
   */
  public function getAll()
  {
    $repo = $this->getDoctrine()->getRepository(Operations::class);
    $operations = $repo->findAll();
    $json = $this->serializer->serialize($operations, "json");

    return JsonResponse::fromJsonString($json);
  }

    /**
     * @Route("/add", methods="POST")
     */
    public function add(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $operations = new Operations();
        $data = json_decode($content, true);

        $operations->setCredit($data["credit"]);
        $operations->setDebit($data["debit"]);
        $operations->setDescription($data["description"]);
        $operations->setDate($data["date"]);

        $manager->persist($operations);
        $manager->flush();

        $data = $this->serializer->normalize($operations);

        $response = JsonResponse::fromJsonString($this->serializer->serialize($operations, "json"));
        return $response;
    }

  /**
   * @Route("/", methods="PUT")
   */
  public function update(Operations $operation, Request $request)
  {

    $body = $request->getContent();
    $updated = $this->serializer->deserialize($body, Operations::class, "json");

    $manager = $this->getDoctrine()->getManager();

    $operation->setDescription($updated->getDescription());
    $operation->setDebit($updated->getDebit());
    $operation->setCredit($updated->getCredit());
    $operation->setDate($updated->getDate());
    $manager->flush();
    return new Response("", 204);
  }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function del(Operations $operations)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($operations);
        $manager->flush();

        $json = $this->serializer->serialize($operations, "json");

        return JsonResponse::fromJsonString($json);
    }

}

